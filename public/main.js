//=============================================================================*

// Main Author: Thomas Guilbaud <thomas.guilbaud@epfl.ch>, EPFL/Transmutex SA (Switzerland)

//=============================================================================*
// Constants

let side = 50;
let margin = 1;

document.documentElement.style.setProperty('--s', side+'px');
document.documentElement.style.setProperty('--m', margin+'px');

const latticeCanvas = document.getElementById("lattice-canvas");
const squareLatticeRowColumnNames = document.getElementById("lattice-canvas-row-column-names");
const selectPattern = document.querySelectorAll('input[name="lattice-type"]');
const showCircle = document.getElementById("show-circle");
const showRowColumnNamesHelper = document.getElementById("show-row-column-names-helper");
const latticeCircleHelper = document.getElementById("lattice-circle-helper-span");
const latticeCircleSliderSize = document.getElementById("lattice-circle-slider-size");
const symmetryMode = document.getElementById("symmetry-mode");
const symmetryNumber = document.getElementById("symmetry-number");
const colorMapContainer = document.getElementById("color-map-grid-content");

let colorMap = '0: #000000:true'; // window.localStorage.getItem('color-map');

//=============================================================================*
// Event listener

// Onclick hexagon
const latticeCanvasPressed = e => { 
    const elementID = e.target.id;

    updateBySymmetryById(elementID);

    changeColorLatticeTileById(elementID);

    updateLatticeTextFromLatticeCanvas();

    cleanColorMapTable();

    generateColorMapTable();

    updateStatistics();
}

latticeCanvas.addEventListener("click", latticeCanvasPressed);


// Highlight column and row labels for square lattices
const letterHighlight = e => {
    const elementID = e.target.id;
    
    if (elementID.includes("h")) {
        const elementCoord = getElementIndicesById(elementID);

        const labelRow = document.getElementById("squareLabelRow"+elementCoord[0]);
        const labelCol = document.getElementById("squareLabelCol"+elementCoord[1]);

        if (e.type === "mouseover") {
            labelRow.style.background = "var(--lightgrey)";
            labelCol.style.background = "var(--lightgrey)";
        }
        else if (e.type === "mouseout") {
            labelRow.style.background = "none";
            labelCol.style.background = "none";
        } 
    }
}

latticeCanvas.addEventListener("mouseover", letterHighlight);
latticeCanvas.addEventListener("mouseout", letterHighlight);


// Onclick pattern selection
const patternSelection = e => {
    updateLatticeCanvasAndColorMapTable();

    updateLatticeTextFromLatticeCanvas();
}

for (const radioButton of selectPattern) 
{
    radioButton.addEventListener('change', patternSelection);
}


showRowColumnNamesHelper.addEventListener('change', function() {
    squareLatticeRowColumnNames.style.display = this.checked ? "block" : "none";
});


// Add circle helper
showCircle.addEventListener('change', function() {
    if (this.checked) {
        latticeCircleHelper.style.display = "block";
        latticeCircleSliderSize.style.display = "block";
        updateLatticeCircleHelper();
    } else {
        latticeCircleHelper.style.display = "none";
        latticeCircleSliderSize.style.display = "none";
    }
});

// Update the current slider value (each time you drag the slider handle)
latticeCircleSliderSize.oninput = function() {
    const sliderCicleSize = latticeCircleSliderSize.value;
    latticeCircleHelper.style.width = sliderCicleSize+"vw";
    latticeCircleHelper.style.height = sliderCicleSize+"vw";

    const latticePitch = document.getElementById("lattice-pitch-input").value;
    const circleSize = 0.5*(sliderCicleSize * window.innerWidth/100) * latticePitch / (side + 2*margin);
    document.getElementById("lattice-circle-helper-input").value = Math.round(circleSize*1e6)/1e6;
}

function updateLatticeCircleHelper()
{
    const latticePitch = document.getElementById("lattice-pitch-input").value;
    const circleSize = document.getElementById("lattice-circle-helper-input").value;
    const size = 2*Math.round(circleSize/latticePitch * (side + 2*margin) * 1e6)/1e6;
    latticeCircleHelper.style.width = size + "px";
    latticeCircleHelper.style.height = size + "px";
    latticeCircleSliderSize.value = size * 100/window.innerWidth;
}

document.getElementById("lattice-pitch-input").addEventListener("change", updateLatticeCircleHelper);
document.getElementById("lattice-circle-helper-input").addEventListener("change", updateLatticeCircleHelper);



// Add symmetry mode input
symmetryMode.addEventListener('change', function() {
    if (this.checked) {
        symmetryNumber.style.display = "block";
    } else {
        symmetryNumber.style.display = "none";
    }
});


// Copy to clipboard
const copyToClipboard = () => {
    const text = document.getElementById('copy-button');
    const icon = document.getElementById('copy-button-icon');

    if (navigator.clipboard) {
        const latticeText = document.getElementById('lattice-text').value;
        navigator.clipboard.writeText(latticeText).then(() => {
            text.innerText = 'Copied!';
            text.style.marginLeft = '0.5rem';
            text.style.marginRight = '0.5rem';
            icon.innerText = 'inventory';
            window.setTimeout(() => {
                text.innerText = '';
                text.style.marginLeft = '0';
                text.style.marginRight = '0';
                icon.innerText = 'content_paste';
            }, 2000);
        });
    } else {
        text.innerText = 'Impossible to copy';
    }
}


//=============================================================================*
// Lattice Manipulation

function decreaseLatticeTileSize()
{
    if (side >= 10) {
        side -= 2;
        document.documentElement.style.setProperty('--s', side+'px');

        if (side < 20) {
            margin = 0;
            document.documentElement.style.setProperty('--m', margin+'px');
        }

        updateLatticeCanvasAndColorMapTable();
    }

    updateLatticeCircleHelper();
}

function increaseLatticeTileSize()
{
    side += 2;
    document.documentElement.style.setProperty('--s', side+'px');

    if (side >= 20) {
        margin = 1;
        document.documentElement.style.setProperty('--m', margin+'px');
    }

    updateLatticeCanvasAndColorMapTable();

    updateLatticeCircleHelper();
}

function increaseLatticeTileSizeToMaximum()
{
    let sidePrev = 0;
    for (let iter = 0; iter < 100; iter++)
    {
        if (sidePrev !== side) {
            sidePrev = side;

            increaseLatticeTileSize();

            resizeLatticeCanvas();
        }
        else {
            break;
        }
    }
}

function resizeLatticeCanvas()
{
    const latticeWidth = latticeCanvas.offsetWidth;
    const totalWidth = document.getElementById("lattice-section").offsetWidth;

    const latticeHeight = latticeCanvas.offsetHeight;
    const totalHeight = document.getElementById("lattice-section").offsetHeight;

    if (latticeWidth + 2*3.2*16 >= totalWidth || latticeHeight + 2*3.2*16 >= totalHeight) {
        decreaseLatticeTileSize();
    }
}

function toggleFullscreen()
{
    const icon = document.getElementById("fullscreen-icon");
    const allSectionsClassList = document.getElementById("all-sections").classList;
    if (icon.innerText === "fullscreen") {
        allSectionsClassList.remove("grid");
        allSectionsClassList.add("grid-fullscreen");
        icon.innerText = "fullscreen_exit";
    } else {
        allSectionsClassList.add("grid");
        allSectionsClassList.remove("grid-fullscreen");
        icon.innerText = "fullscreen";
    }
    resizeLatticeCanvas();

    updateLatticeCircleHelper();
}

function toggleLatticeParametersDropdown()
{
    const icon = document.getElementById("icon-dropdown-lattice-parameters").innerText; 
    if (icon == "keyboard_arrow_down") {
        document.getElementById("lattice-parameters-body").style.display = "block";
        document.getElementById("icon-dropdown-lattice-parameters").innerText = "keyboard_arrow_up";
    }
    else {
        document.getElementById("lattice-parameters-body").style.display = "none";
        document.getElementById("icon-dropdown-lattice-parameters").innerText = "keyboard_arrow_down";
    }
}

function addColLeft()
{
    const text = document.getElementById('lattice-text').value.trim()
        .split('\n')
        .map(e => e.trim())
        .map(e => '0 ' + e)
        .join('\n');

    document.getElementById('lattice-text').value = text;

    updateLatticeCanvasAndColorMapTable();
    updateLatticeTextFromLatticeCanvas();
}

function removeColLeft()
{
    const text = document.getElementById('lattice-text').value
        .trim()
        .split('\n')
        .map(e => e.trim().split(' ').slice(1).join(' '))
        .join('\n');

    document.getElementById('lattice-text').value = text;

    updateLatticeCanvasAndColorMapTable();
    updateLatticeTextFromLatticeCanvas();
}

function addColRight()
{
    const text = document.getElementById('lattice-text').value
        .trim()
        .split('\n')
        .map(e => e.trim())
        .map(e => e + ' 0')
        .join('\n');

    document.getElementById('lattice-text').value = text;

    updateLatticeCanvasAndColorMapTable();
    updateLatticeTextFromLatticeCanvas();
}

function removeColRight()
{
    const text = document.getElementById('lattice-text').value
        .trim()
        .split('\n')
        .map(e => e.trim().split(' ').slice(0, -1).join(' '))
        .join('\n');

    document.getElementById('lattice-text').value = text;

    updateLatticeCanvasAndColorMapTable();
    updateLatticeTextFromLatticeCanvas();
}

function addRowTop()
{
    let text = document.getElementById('lattice-text').value;

    const textLength = text.split('\n')[0].split(' ').length;
    text = '\n' + text;
    for (let i = 0; i < textLength; i++)
    {
        text = '0 '+text;
    }

    document.getElementById('lattice-text').value = text;

    updateLatticeCanvasAndColorMapTable();
    updateLatticeTextFromLatticeCanvas();
}

function removeRowTop()
{
    const text = document.getElementById('lattice-text').value
        .trim()
        .split('\n')
        .slice(1)
        .join('\n');

    document.getElementById('lattice-text').value = text;

    updateLatticeCanvasAndColorMapTable();
    updateLatticeTextFromLatticeCanvas();
}

function addRowBottom()
{
    let text = document.getElementById('lattice-text').value;

    const textLength = text.split('\n')[0].split(' ').length;
    text += '\n';
    for (let i = 0; i < textLength; i++)
    {
        text += '0 ';
    }

    document.getElementById('lattice-text').value = text;

    updateLatticeCanvasAndColorMapTable();
    updateLatticeTextFromLatticeCanvas();
}

function removeRowBottom()
{
    const text = document.getElementById('lattice-text').value
        .trim()
        .split('\n')
        .slice(0, -1)
        .join('\n');

    document.getElementById('lattice-text').value = text;

    updateLatticeCanvasAndColorMapTable();
    updateLatticeTextFromLatticeCanvas();
}

function changeColorLatticeTileById(elementID)
{
    const element = document.getElementById(elementID);
    
    if (element.tagName != 'BUTTON')
    {
        return;
    }

    const currentValue = element.innerHTML;

    const colors = extractColorListFromColorMap();
    const labels = extractLabelListFromColorMap();
    const checkboxes = extractCheckedListFromColorMap();

    colorsFiltered = colors.filter((e, idx) => checkboxes[idx]);
    labelsFiltered = labels.filter((e, idx) => checkboxes[idx]);

    const idx = labelsFiltered.indexOf(currentValue);
    const newIdx = (idx+1) % colorsFiltered.length;

    // Update
    element.style.background = colorsFiltered[newIdx];
    element.innerHTML = labelsFiltered[newIdx];
}

function cleanLatticeCanvas()
{
    document.getElementById("lattice-canvas").innerHTML = '';
}

function generateLatticeCanvasFromLatticeText()
{
    const latticeTextAsArray = getLatticeTextAsArray()

    for (let row = 0; row < getNumberRow(); row++)
    {
        let newRow = document.createElement('div');
        newRow.className = isHexagonPattern() ? 'row-hexagon' : 'row-square';
        newRow.id = 'row' + row;
        if (isHexagonPattern()) {
            newRow.style = 'margin-left: '+ row*(side/2 + margin) + 'px;'
        }
        latticeCanvas.appendChild(newRow);

        for (let col = 0; col < latticeTextAsArray[row].length; col++)
        {
            const value = latticeTextAsArray[row][col];
            let newDiv = document.createElement('button');
            newDiv.id = 'row'+row+'-h'+col;
            newDiv.innerHTML = value;
            newDiv.style.background = getColorByLabel(value);
            newDiv.style.padding = 0;
            newRow.appendChild(newDiv);
        }
    }


    // Names of row and columns for square lattices
    squareLatticeRowColumnNames.innerHTML = '';
    squareLatticeRowColumnNames.style.display = showRowColumnNamesHelper.checked ? 'block' : 'none';
    
    for (let row = 0; row < getNumberRow()+2; row++)
    {
        let newRow = document.createElement('div');
        newRow.className = isHexagonPattern() ? 'row-hexagon' : 'row-square';
        if (isHexagonPattern()) {
            newRow.style = 'margin-left: '+ row*(side/2 + margin) + 'px;'
        }
        squareLatticeRowColumnNames.appendChild(newRow);

        for (let col = 0; col < getNumberColumn()+2; col++)
        {
            let newDiv = document.createElement('button');
            if (row === 0 && 0 < col && col <= getNumberColumn()) { // Column letter
                if (col > 26) {
                    newDiv.innerHTML = String.fromCharCode(65-1+(col - ((col-1) % 26))/26);
                }
                newDiv.innerHTML += String.fromCharCode(65+(col-1) % 26);
                newDiv.id = "squareLabelCol"+(col-1);
                newDiv.style.color = 'black';
            }
            else if (0 < row && row <= getNumberRow() && col === 0) { // Line number
                newDiv.innerHTML = row;
                newDiv.id = "squareLabelRow"+(row-1);
                newDiv.style.color = 'black';
            }
            else {
                newDiv.innerHTML = '.';
                newDiv.style.color = 'transparent';
            }
            newDiv.style.background = 'none';
            newDiv.style.pointerEvents = 'none';
            newDiv.style.padding = 0;
            newRow.appendChild(newDiv);
        }
    }
}

function updateLatticeCanvasAndColorMapTable()
{
    updateColorMapFromLatticeText();

    cleanLatticeCanvas();
    cleanColorMapTable();
    
    generateLatticeCanvasFromLatticeText();

    generateColorMapTable();
    
    resizeLatticeCanvas();

    updateStatistics();
}

function updateBySymmetryById(elementID)
{
    if (symmetryMode.checked)
    {
        const elementCoord = getElementIndicesById(elementID);

        const nSymmetry = symmetryNumber.value;
        const theta = 2*Math.PI / nSymmetry;
        
        if (isHexagonPattern())
        {
            const coord = computeCoordinates();

            const idx0 = elementCoord[0];
            const idy0 = elementCoord[1];

            if (isNaN(idx0) || isNaN(idy0)) {
                return;
            }

            const x = coord[idx0][idy0][0];
            const y = coord[idx0][idy0][1];

            for (let i = 1; i < nSymmetry; i++)
            {
                const newX = x * Math.cos(i * theta) - y * Math.sin(i * theta);
                const newY = x * Math.sin(i * theta) + y * Math.cos(i * theta);

                for (let j = 0; j < coord.length; j++)
                {
                    for (let k = 0; k < coord[j].length; k++)
                    {
                        if (Math.abs(coord[j][k][0] - newX) < 1e-9 && Math.abs(coord[j][k][1] - newY) < 1e-9 && (idx0 != j || idy0 != k))
                        {
                            const newElementID = "row"+j+"-h"+k;
                            changeColorLatticeTileById(newElementID);
                            break;
                        }
                    }
                }
            }
        }
        else
        {
            const offsetX = (getNumberRow()-1)/2;
            const offsetY = (getNumberColumn()-1)/2;

            const x = elementCoord[0] - offsetX;
            const y = elementCoord[1] - offsetY;

            for (let i = 1; i < nSymmetry; i++)
            {
                const newX = Math.round(x * Math.cos(i*theta) - y * Math.sin(i*theta) + offsetX);
                const newY = Math.round(x * Math.sin(i*theta) + y * Math.cos(i*theta) + offsetY);
                
                if (newX != elementCoord[0] || newY != elementCoord[1]) {
                    const newElementID = "row"+newX+"-h"+newY;
                    changeColorLatticeTileById(newElementID);
                }
            }
        }
    }
}

//=============================================================================*
// Color map table

function initDefaultColorMap()
{
    colorMap = 'F: #E17070:true; W: #5882E3:true; 0: #000000:true';
}

function cleanColorMapTable()
{
    document.getElementById("color-map-grid-content").innerHTML = '';
}

function colorMapTableInputLabelOnKeyUp(event)
{
    if (event.target.value.trim() !== '') {
        replaceLabelInLatticeText(event.target.defaultValue, event.target.value.trim());

        event.target.defaultValue = event.target.value.trim();

        updateColorMapFromTable();
        cleanLatticeCanvas();
        generateLatticeCanvasFromLatticeText();
    }
}

function colorMapTableInputLabelOnFocus(event)
{
    event.target.defaultValue = event.target.value.trim();
}

function colorMapTableInputLabelOnBlur(event)
{
    if (event.target.value.trim() === '') {
        event.target.value = event.target.defaultValue.trim();
    }

    cleanColorMapTable();
    generateColorMapTable();
}

function colorMapTableInputColorOnChange()
{
    updateColorMapFromTable();
    cleanLatticeCanvas();
    generateLatticeCanvasFromLatticeText();
}

function sortColorMap()
{
    colorMap = colorMap
        .split('; ')
        .filter((e) => e.split(': ')[0] != '0') // Filter non '0' elements
        .concat(
            colorMap
                .split('; ')
                .filter((e) => e.split(': ')[0] == '0') // Filter '0' elements
        )
        .join('; ');
}

function toggleRowColorMap(e)
{
    const targetIdx = parseInt(e.target.id.split('-')[2]);
    const targetChecked = e.target.checked;

    colorMap = colorMap.trim()
        .split(';')
        .map((e, idx) => {
            if (idx === targetIdx) {
                let temp = e.split(':');
                temp[2] = targetChecked;
                return(temp.join(':'));
            } else {
                return(e);
            }
        })
        .join('; ');
}

function addRowColorMap()
{
    const elementList = extractLabelListFromColorMap();

    const isAllNumbers = elementList.every((e) => !isNaN(parseInt(e)));

    let newElement = ".";
    if (isAllNumbers) {
        const lastNumber = Math.max(...elementList.map((e) => parseInt(e)));

        newElement = lastNumber+1;
    }
    else {
        const maxLength = Math.max(...elementList.map((e) => e.length));
        const isAllLowercase = elementList.every((e) => e === e.toLowerCase());
        const isAllUppercase = elementList.every((e) => e === e.toUpperCase());
        let nIter = 0;
        do {
            newElement = generateRandomString(maxLength);
            if (isAllLowercase) {
                newElement = newElement.toLowerCase();
            }
            if (isAllUppercase) {
                newElement = newElement.toUpperCase();
            }
            nIter++;
        }
        while (elementList.includes(newElement) && nIter < 100)

        if (nIter >= 100) {
            newElement = generateRandomString(maxLength+1);
        }
    }
    
    colorMap += "; "+newElement+": "+getRandomColor()+":true";

    sortColorMap();

    cleanColorMapTable();
    generateColorMapTable();
}

function deleteColorMapRow(e)
{
    const idxToDelete = parseInt(e.target.id.split("-")[2]);
    const labelToDelete = extractLabelListFromColorMap()[idxToDelete];

    colorMap = colorMap
        .split('; ')
        .filter((e, idx) => idx !== idxToDelete)
        .join('; ');

    replaceLabelInLatticeText(labelToDelete, '0');

    cleanColorMapTable();
    generateColorMapTable();

    updateLatticeCanvasAndColorMapTable();
}

function generateColorMapTable()
{
    sortColorMap();

    const labels = extractLabelListFromColorMap();
    const colors = extractColorListFromColorMap();
    const checked = extractCheckedListFromColorMap();

    const container = document.getElementById("color-map-grid-content");

    // Create rows
    labels.forEach((label, idx) => {
        // Create the elements
        let checkButtonDisplay = document.createElement('input');
        let inputLabel = document.createElement('input');
        let inputColor = document.createElement('input');
        let countText = document.createElement('p');
        let deleteButton = document.createElement('button');

        // Attributes for the checkbox
        checkButtonDisplay.id = "select-row-" + idx;
        checkButtonDisplay.type = "checkbox";
        checkButtonDisplay.checked = checked[idx];
        checkButtonDisplay.className = "color-map-checkbox";
        checkButtonDisplay.onchange = toggleRowColorMap;

        // Attirbutes for the label selection
        inputLabel.value = label;
        inputLabel.onfocus = colorMapTableInputLabelOnFocus;
        inputLabel.onkeyup = colorMapTableInputLabelOnKeyUp;
        inputLabel.onblur = colorMapTableInputLabelOnBlur;
        inputLabel.className = "color-map-label";
        inputLabel.ariaLabel = "Color map element name input";

        // Attirbutes for the color selection
        inputColor.value = colors[idx];
        inputColor.type = 'color';
        inputColor.onchange = colorMapTableInputColorOnChange;
        inputColor.className = "color-map-color";
        inputColor.ariaLabel = "Color map element color input";

        // Attirbutes for the count text
        countText.innerText = getNumberLabelInLatticeTextByLabel(label);

        // Attirbutes for the delete button
        deleteButton.id = "delete-row-" + idx;
        deleteButton.ariaLabel = "Delete row in color map list";
        if (label !== "0") {
            deleteButton.className = "color-map-delete-row";
            deleteButton.onclick = deleteColorMapRow;
        } else {
            deleteButton.className = "color-map-delete-row-zero";
            deleteButton.disabled = true;
        }

        // Append the elements in the row
        container.appendChild(checkButtonDisplay);
        container.appendChild(inputLabel);
        container.appendChild(inputColor);
        container.appendChild(countText);
        container.appendChild(deleteButton);
    });
}

function updateColorMapFromTable()
{
    const currentList = Array.from(
        document.getElementById("color-map-grid-content").children
    );

    if (currentList && currentList.length > 0)
    {
        colorMap = currentList
            .filter(e => e?.className === "color-map-label" || e?.className === "color-map-color")
            .reduce((acc, e, idx) =>
                acc + e.value.toString() + (idx % 2 ? ":"+extractCheckedListFromColorMap()[(idx-1)/2]+'; ' : ': '), ''
            );
        colorMap = colorMap.slice(0, colorMap.length-2);
    }
}

function updateColorMapFromLatticeText()
{
    updateColorMapFromTable();

    const labels = extractLabelListFromColorMap();
    const colors = extractColorListFromColorMap();

    const labelsFromLatticeText = document.getElementById('lattice-text').value
        .trim()
        .split('\n')
        .map(e => e.trim().split(' ').filter(e => e !== ""))
        .reduce((acc, curr) => [...acc, ...curr], []);

    const isLabelAlreadyInList = labelsFromLatticeText.every(label => labels.includes(label));
    
    if (!isLabelAlreadyInList)
    {
        const newColorMap = labels
            .concat(labelsFromLatticeText)
            .filter((value, index, self) => self.indexOf(value) === index)
            .map((e, idx) => e+": "+(colors[idx] ? colors[idx] : getRandomColor()))
            .join("; ");

        colorMap = newColorMap;
    }
}

//=============================================================================*
// Usefull functions

function getRandomColor()
{
    return("#"+parseInt(Math.random() * 0xffffff).toString(16));
}

function isHexagonPattern()
{
    for (const radioButton of selectPattern)
    {
        if (radioButton.checked && radioButton.value == 'Hexagon') {
            return(true);
        }
    }
    return(false);
}

function getLatticeTextAsArray()
{
    return(
        document.getElementById('lattice-text').value
            .trim()
            .split('\n')
            .filter(e => e !== "")
            .map(row => row
                .trim()
                .split(' ')
                .filter(e => e !== "")
            )
    );
}

function getNumberRow()
{
    return(getLatticeTextAsArray().length);
}

function getNumberColumn()
{
    return(getLatticeTextAsArray()[0].length);
}

function isRegularLattice()
{
    const latticeTextAsArray = getLatticeTextAsArray();
    const nRows = getNumberRow();
    const sizeRow1 = getNumberColumn();
    for (let i = 1; i < nRows; i++)
    {
        if (latticeTextAsArray[i].length != sizeRow1) {
            return(false);
        }
    }
    return(true);
}

function extractColorListFromColorMap()
{
    return(
        colorMap.trim()
            .split(';')
            .map(e => e.includes(':') ? e.split(':')[1] : e)
            .map(e => e.trim())
    );
}

function generateRandomString(length)
{
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function extractLabelListFromColorMap()
{
    return(
        colorMap.trim()
            .split(';')
            .map(e => e.split(':')[0].trim())
            .filter(e => e !== '')
    );
}

function extractCheckedListFromColorMap()
{
    return(
        colorMap.trim()
            .split(';')
            .map(e => e.includes(':') ? e.split(':')[2] : e)
            .map(e => e.trim())
            .map(e => e === 'true')
    );
}

function getColorByLabel(label)
{
    const colors = extractColorListFromColorMap();
    const labels = extractLabelListFromColorMap();
    const idx = labels.indexOf(label);
    return(colors[idx]);
}

function replaceLabelInLatticeText(oldLabel, newLabel)
{
    let latticeText = document.getElementById('lattice-text');
    latticeText.value = latticeText.value
        .trim()
        .split('\n')
        .filter(e => e !== "")
        .map(row => row
            .split(' ')
            .filter(e => e !== "")
            .map(e => e === oldLabel ? newLabel : e)
            .join(' ')
            .trim()
        )
        .map((row, idx) => (isHexagonPattern() ? " ".repeat(idx) : '') + row)
        .join('\n')
        .trim();
}

function updateLatticeTextFromLatticeCanvas()
{
    let text = '';

    const rows = latticeCanvas.children;
    for (let i = 0; i < rows.length; i++) 
    {
        const columns = rows[i].children;
        for (let j = 0; j < columns.length; j++)
        {
            const innerHTML = columns[j].innerHTML;
            text += innerHTML == "" ? '0' : innerHTML;

            if (j < columns.length-1) {
                text += ' ';
            }
        }

        if (i < rows.length-1) {
            text += '\n';
        
            // Add indentation
            if (isHexagonPattern()) {
                text += ' '.repeat(i+1);
            }
        }
    }

    document.getElementById('lattice-text').value = text;
}

function computeCoordinates()
{
    let list = [];
    if (isRegularLattice())
    {
        const nRows = getNumberRow();
        const nCols = getNumberColumn();
        const pitch = 1;

        if (isHexagonPattern())
        {
            summitToSummit = pitch * 2/Math.sqrt(3);
            for (let i = 0; i < nRows; i++)
            {
                temp = [];
                for (let j = 0; j < nCols; j++)
                {
                    const x = summitToSummit * 0.75 * (i - (nRows-1)/2);
                    const y = pitch * (j - (nCols-1)/2);
                    temp.push([x, y + (i - (nRows-1)/2)*pitch/2]);
                }
                list.push(temp);
            }
        }
        else
        {
            for (let i = 0; i < nRows; i++)
            {
                temp = [];
                for (let j = 0; j < nCols; j++)
                {
                    const x = pitch * (i - (nRows-1)/2);
                    const y = pitch * (j - (nCols-1)/2);
                    temp.push([x, y]);
                }
                list.push(temp);
            }
        }
    }
    return(list);
}

function isSymmetric()
{
    const coordinates = computeCoordinates();
    if (isRegularLattice() && coordinates.length > 0)
    {
        const latticeTextAsArray = getLatticeTextAsArray();
    
        let avgX = 0;
        let avgY = 0;
        for (let i = 0; i < getNumberRow(); i++)
        {
            for (let j = 0; j < getNumberColumn(); j++)
            {
                const x = coordinates[i][j][0];
                const y = coordinates[i][j][1];
                if (latticeTextAsArray[i][j] != '0')
                {
                    avgX += x * latticeTextAsArray[i][j].charCodeAt(0);
                    avgY += y * latticeTextAsArray[i][j].charCodeAt(0);
                }
            }
        }

        if (Math.abs(avgX) < 1e-6 && Math.abs(avgY) < 1e-6) {
            return(true)
        }
    }
    return(false)
}

function getElementIndicesById(id)
{
    const row = parseInt(id.split("-h")[0].split("row")[1]);
    const col = parseInt(id.split("-h")[1]);
    return([row, col]);
}


//=============================================================================*
// Statistics and Checks

function getNumberLabelInLatticeTextByLabel(label)
{
    return(
        getLatticeTextAsArray().flat().filter(e => e == label).length
    );
}

function updateStatistics()
{
    const labels = extractLabelListFromColorMap().filter(e => e != '0');
    
    // Total labels
    const totalElement = getLatticeTextAsArray()
        .flat()
        .filter(e => labels.includes(e))
        .length;

    document.getElementById("stat-total-values").innerHTML = 
        'Unique elements: ' + labels.length + ' | Total placed: ' + totalElement;
        
    // Lattice size, shape and symmetry
    let latticeSize = '';
    if (!isRegularLattice()) {
        latticeSize += "Non regular shape, ";
    }
    if (isSymmetric()) {
        // latticeSize += "Symmetric, "
        document.getElementById("lattice-symmetry-flag").style.display = "block";
    }
    else { 
        document.getElementById("lattice-symmetry-flag").style.display = "none";
    }
    latticeSize += getNumberRow() + ' rows | ' + getNumberColumn() + ' cols';
    document.getElementById("stat-lattice-size").innerHTML = latticeSize;
}


//=============================================================================*
// Initial setup and default values

initDefaultColorMap();

cleanLatticeCanvas();
cleanColorMapTable();

generateLatticeCanvasFromLatticeText();

resizeLatticeCanvas();

generateColorMapTable();

updateStatistics();

//=============================================================================*
