# Honeycomb

Honeycomb is a web application to create, visualize, edit, import and export lattices. The primary objective is to facilitate the modification of lattice for the nuclear engineering community, which are commonly used in Monte Carlo codes such as MCNP or Serpent.

The application is accessible at the following URL: https://foam-for-nuclear.gitlab.io/honeycomb.


## Run the app in local

Open the [`public/index.html`](./public/index.html) in your favorite web browser.


## Gallery

![](./public/images/pwrLatticeExample.png)

*Fig 1: Example of a core lattice pattern for a PWR.*


![](./public/images/pwrAssemblyLatticeExample.png)

*Fig 2: Example of a fuel assembly lattice pattern for a PWR.*


![](./public/images/sfrLatticeExample.png)

*Fig 3: Example of a core lattice pattern for an SFR.*


<img src="./public/images/ntpLatticeExample.png" width=600px/>

*Fig 4: Example of a core lattice pattern for an NTP.*


## Contributors

Thomas Guilbaud, EPFL/Transmutex SA, Main author  
Marion Séminel, Style
